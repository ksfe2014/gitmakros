/**
 @author: Frederic Gehmlich
 @created: 2014-02-18
 @content: Ruft alle SAS-Programme im Unterverzeichnis macros auf.
  
 **/
OPTIONS noxwait;
%INCLUDE ".\makros\*.sas";
