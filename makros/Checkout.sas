/**
 @author: Frederic Gehmlich
 @content: Wechselt zu einem anderen Branch/Zweig.
           Falls der Branch nicht existiert, wird ein neuer erzeugt.

 @note:    In diesem Makro wird Git asynchron aufgerufen.
           Falls Sie dieses Makro mit Tastatur-Hotkeys kombinieren wollen,
           m�ssen Sie "systask command" durch "x" ersetzen.
 **/
%MACRO checkout(branch);
options nonotes;
systask command "git checkout -B &branch. ";
options notes;
%MEND;
