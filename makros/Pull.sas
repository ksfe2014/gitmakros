/**
 @author: Frederic Gehmlich
 @content: Gleicht das lokale Repository mit dem Remote-Repository ab.  
 **/

%MACRO pull;
x "git pull origin";
%MEND;
