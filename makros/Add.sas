/**
 @author: Frederic Gehmlich
 @content: F�gt neue oder ge�nderte Dateien der Staging-Area hinzu.
 **/
%MACRO add(file);
%IF %LENGTH(&file.) EQ 0 %THEN %DO;
    x "git add . ";
%END;
%ELSE %DO;
    x "git add &file. ";
%END;
%MEND; 

