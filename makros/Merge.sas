/**
 @author: Frederic Gehmlich
 @content: Vereint einen Zweig mit einem anderen.
           Standardwert von Variable toBranch ist master.

 @note:    In diesem Makro wird Git asynchron aufgerufen.
           Falls Sie dieses Makro mit Tastatur-Hotkeys kombinieren wollen,
           m�ssen Sie "systask command" durch "x" ersetzen.
 **/
%MACRO merge(fromBranch, toBranch);
options nonotes;
%IF %LENGTH(&toBranch.) EQ 0 %THEN %DO;
    %LET toBranch=master;
%END;
%IF %LENGTH(&fromBranch.) EQ 0 %THEN %DO;
    %PUT NOTE: Bitte geben Sie den Zweig an, welchen Sie mit &toBranch. mergen m�chten.;
    %RETURN;
%END;

systask command "git checkout &toBranch. " wait;
systask command "git merge &fromBranch. " wait;

%IF &fromBranch. NE develop AND 
    &fromBranch. NE master %THEN %DO;
    systask command "git branch -d &fromBranch. ";
%END;
options notes;
%MEND;
