/**
 @author: Frederic Gehmlich
 @content: Alle Dateien, die zur Stage hinzugefügt wurden, 
           werden an das entsprechende Repository übergeben.
 **/
%MACRO commit(msg);
 
%IF %LENGTH(&msg.) EQ 0 %THEN %DO;
    %LET msg = Committed via SAS.;
%END;
x "git commit -m ""&msg.""";
 
%MEND;
