/**
 @author:  Frederic Gehmlich
 @content: Falls �nderungen versehentlich im falschen Zweig 
           vorgenommen wurden, k�nnen die �nderungen mit %stash(0);
           in einen Zwischenspeicher gelegt werden, um sie anschlie�end
           mit %stash(1); in den richtigen Entwicklungszweig abzulegen. 

 @note:    In diesem Makro wird Git asynchron aufgerufen.
           Falls Sie dieses Makro mit Tastatur-Hotkeys kombinieren wollen,
           m�ssen Sie "systask command" durch "x" ersetzen.
 **/
%MACRO stash(value);
options nonotes;
%IF &value. EQ 0 %THEN %DO;
   systask command "git stash ";
%END;
%ELSE %IF &value. EQ 1 %THEN %DO;
   systask command "git stash pop ";  
%END;
%ELSE %DO;
    options notes;
    %PUT NOTE: Bitte �berpr�fen Sie den Eingabeparameter.;
    %PUT NOTE- stash(0) = �nderungen in Zwischenspeicher verschieben.;
    %PUT NOTE- stahs(1) = �nderungen in aktuellen Zweig ablegen.;
    %RETURN;
%END;
options notes;

%MEND;
